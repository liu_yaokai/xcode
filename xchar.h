//
// Created by "Yaokai Liu" on 12/12/22.
//

#ifndef X_XCHAR_H
#define X_XCHAR_H

#include "__char_def.h"
#include "xtypes.h"
#include "xdef.h"

#ifdef BASIC_FORM
#undef BASIC_FORM
#endif
#define BASIC_FORM  ((xSize)0)


typedef xByte   ascii;
typedef xuByte  xAscii;

xAscii  a2x(ascii _ascii);
ascii   x2a(xAscii _x_ascii);

typedef struct {
    xSize  size: 4;
    xSize  form: 4;
} xCharHeader;

typedef struct {
    xCharHeader header;
    xuByte  shape[];
} xChar;

xuInt ord(xChar * _char);
xVoid chr(xuInt _order, xChar * _char, xuByte _form);

xVoid a2xc(xByte _ascii, xChar * _char);
xByte xc2a(xChar * _char);

#endif //X_XCHAR_H
