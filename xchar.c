//
// Created by "Yaokai Liu" on 12/12/22.
//


#include "ascii-xcode.cdata"
#define min(_a, _b)     ((_a < _b) ? (_a) : (_b))

xAscii  a2x(ascii _ascii) {
    return (_ascii != 0) ? A2XC_TABLE[_ascii].shape[0] : XCHAR_CNUL;
}

ascii   x2a(xAscii _x_ascii) {
    return XC2A_VC_TABLE[_x_ascii];
}

xuInt ord(xChar * _char) {
    xuInt order = 0;
    for (xSize i = 0;i < min(_char->header.size, sizeof(xuInt)); i ++) {
        order += ((xuInt) _char->shape[i]) << (i * 8);
    }
    return order;
}

xVoid chr(xuInt _order, xChar * _char, xuByte _form) {
    _char->header.form = _form;
    #define i   (_char->header.size)
    for(i = 0; i < sizeof(xuInt); i ++) {
        _char->shape[i] = (xuByte) (_order >> (i * 8));
    }
    #undef i
}

xVoid a2xc(xByte _ascii, xChar * _char) {
    if (_ascii < 0) {
        return;
    }
    _char->header.size = A2XC_TABLE[_ascii].header.size;
    _char->header.form = A2XC_TABLE[_ascii].header.form;
    _char->shape[0] = A2XC_TABLE[_ascii].shape[0];
}

xByte xc2a(xChar * _char) {
    if (_char->header.size == 0) {
        return XC2A_WS_TABLE[_char->header.form];
    } else if (_char->header.size == 1) {
        return XC2A_VC_TABLE[_char->shape[0]];
    } else {
        return -1;
    }
}

