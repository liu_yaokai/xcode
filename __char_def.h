//
// Created by "Yaokai Liu" on 12/15/22.
//

#ifndef XC___CHAR_DEF_H
#define XC___CHAR_DEF_H

#include "xdef.h"

#define XCHAR_NUL   NULL
#define XCHAR_LF    NULL
#define XCHAR_CR    NULL
#define XCHAR_HT    NULL

#define XCHAR_0     0
#define XCHAR_1     1
#define XCHAR_2     2
#define XCHAR_3     3
#define XCHAR_4     4
#define XCHAR_5     5
#define XCHAR_6     6
#define XCHAR_7     7
#define XCHAR_8     8
#define XCHAR_9     9

#define XCHAR_A     10
#define XCHAR_B     11
#define XCHAR_C     12
#define XCHAR_D     13
#define XCHAR_E     14
#define XCHAR_F     15
#define XCHAR_G     16
#define XCHAR_H     17
#define XCHAR_I     18
#define XCHAR_J     19
#define XCHAR_K     20
#define XCHAR_L     21
#define XCHAR_M     22
#define XCHAR_N     23
#define XCHAR_O     24
#define XCHAR_P     25
#define XCHAR_Q     26
#define XCHAR_R     27
#define XCHAR_S     28
#define XCHAR_T     29
#define XCHAR_U     30
#define XCHAR_V     31
#define XCHAR_W     32
#define XCHAR_X     33
#define XCHAR_Y     34
#define XCHAR_Z     35

#define XCHAR_a     36
#define XCHAR_b     37
#define XCHAR_c     38
#define XCHAR_d     39
#define XCHAR_e     40
#define XCHAR_f     41
#define XCHAR_g     42
#define XCHAR_h     43
#define XCHAR_i     44
#define XCHAR_j     45
#define XCHAR_k     46
#define XCHAR_l     47
#define XCHAR_m     48
#define XCHAR_n     49
#define XCHAR_o     50
#define XCHAR_p     51
#define XCHAR_q     52
#define XCHAR_r     53
#define XCHAR_s     54
#define XCHAR_t     55
#define XCHAR_u     56
#define XCHAR_v     57
#define XCHAR_w     58
#define XCHAR_x     59
#define XCHAR_y     60
#define XCHAR_z     61

#define XCHAR_U_ALPHA   129
#define XCHAR_U_BETA    130
#define XCHAR_U_GAMMA   131
#define XCHAR_U_DELTA   132
#define XCHAR_U_EPSILON 133
#define XCHAR_U_ZETA    134
#define XCHAR_U_ETA     135
#define XCHAR_U_THETA   136
#define XCHAR_U_IOTA    137
#define XCHAR_U_KAPPA   138
#define XCHAR_U_LAMBDA  139
#define XCHAR_U_MU      140
#define XCHAR_U_NU      141
#define XCHAR_U_XI      142
#define XCHAR_U_OMICRON 143
#define XCHAR_U_PI      144
#define XCHAR_U_RHO     145
#define XCHAR_U_SIGMA   146
#define XCHAR_U_TAO     147
#define XCHAR_U_UPSILON 148
#define XCHAR_U_PHI     149
#define XCHAR_U_CHI     150
#define XCHAR_U_PSI     151
#define XCHAR_U_OMEGA   152

#define XCHAR_L_ALPHA   153
#define XCHAR_L_BETA    154
#define XCHAR_L_GAMMA   155
#define XCHAR_L_DELTA   156
#define XCHAR_L_EPSILON 157
#define XCHAR_L_ZETA    158
#define XCHAR_L_ETA     159
#define XCHAR_L_THETA   160
#define XCHAR_L_IOTA    161
#define XCHAR_L_KAPPA   162
#define XCHAR_L_LAMBDA  163
#define XCHAR_L_MU      164
#define XCHAR_L_NU      165
#define XCHAR_L_XI      166
#define XCHAR_L_OMICRON 167
#define XCHAR_L_PI      168
#define XCHAR_L_RHO     169
#define XCHAR_L_SIGMA   170
#define XCHAR_L_TAO     171
#define XCHAR_L_UPSILON 172
#define XCHAR_L_PHI     173
#define XCHAR_L_CHI     174
#define XCHAR_L_PSI     175
#define XCHAR_L_OMEGA   176

#define XCHAR_SOH   201
#define XCHAR_STX   202
#define XCHAR_ETX   203
#define XCHAR_EOT   204
#define XCHAR_ENQ   205
#define XCHAR_ACK   206
#define XCHAR_BEL   207
#define XCHAR_BS    208
#define XCHAR_VT    209
#define XCHAR_FF    210
#define XCHAR_SO    211
#define XCHAR_SI    212
#define XCHAR_DLE   213
#define XCHAR_DC1   214
#define XCHAR_DC2   215
#define XCHAR_DC3   216
#define XCHAR_DC4   217
#define XCHAR_NAK   218
#define XCHAR_SYN   219
#define XCHAR_TB    220
#define XCHAR_CAN   221
#define XCHAR_EM    222
#define XCHAR_SUB   223
#define XCHAR_ESC   224
#define XCHAR_FS    225
#define XCHAR_GS    226
#define XCHAR_RS    227
#define XCHAR_US    228
#define XCHAR_DEL   229

#define XCHAR_CSPC  251
#define XCHAR_CTBL  252
#define XCHAR_CLFL  253
#define XCHAR_CCRL  254
#define XCHAR_CNUL  255

#endif //XC___CHAR_DEF_H
